# Reverse Linux VNC for GitLab CI
Enable VNC access on GitLab CI/CD VMs for general use.

![xd](screenshot.png)

## Usage
1) Create an account on [ngrok](https://dashboard.ngrok.com/signup), and copy your auth token displayed [here](https://dashboard.ngrok.com/auth)
2) Fork this repository
3) Go to Settings > CI/CD, expand "Runners" and be sure that the option "Enable shared runners for this project" is checked
4) On the same page, expand "Artifacts", uncheck "Keep artifacts from most recent successful jobs"
5) On the same page, expand "Variables", and click on "Add variable", create the following variables:
```
Key: NGROK_AUTH_TOKEN
Value: (obviously you insert the ngrok auth token of your account that you obtained in step 1)
Flags: Protect, Mask

Key: VNC_PASSWORD
Value: (The VNC password that you will be using, if exceeds more than 8 characters, it will be truncated)
Flags: Protect, Mask

Name: VNC_DEPTHVALUE
Value: 16 (for example)
Flags: Protect
This is the default color depth that the VNC will be using, the minimum acceptable value is 8
But i recommend you to set it at 16

Name: VNC_SCREENSIZE
Value: 800x600 (for example)
Flags: Protect
This will be the screen resolution in your VNC session, i don't know a minimal value but it's up to you anyway.
```
All of those variables are required for the script to work, if you forget to add one, then it will throw an error

6) Trigger an build, by editing this README or uploading anything to your repository, don't modify the contents of the resources or scripts folders
7) Go to CI/CD > Jobs, open the running job (might be labeled in blue), and wait until the last step where it will hang forever
8) Visit ngrok's tunnel list [dashboard](https://dashboard.ngrok.com/status/tunnels)
9) Take note of the active tunnel host and port
10) Connect to the host and port combination with a VNC client of your preference
11) If it requires an username, write "root" and as password, the password that you wrote in the VNC_PASSWORD secret in step 5
12) Once connected, select "Use default panel layout" and Enjoy!

## Duration of the runners
You can see how long the runner will last on CI/CD > Jobs, click on your active job and near the log you will see a column with the details of the job, the value under Timeout defines how long it will last (generally for one hour).

## Some issues
- Web browsers will crash if you load heavy websites, or multiple tabs
- I/O operations might throw an error in rare cases
- APT Package Manager has a slightly reduced list on what you might install or not, if you can't install something, search the .deb and install it manually